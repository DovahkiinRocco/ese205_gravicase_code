# My project's README
This is the accelerometer (and gyroscope) code for Gravicase!
Important Note, most of this code was not written by me but taken from a public domain online source at 
http://dummyscodes.blogspot.com/2014/07/mpu6050-gy-521-breakout-arduino-mega.html 
The original author was arduino.cc user "Krodal".
